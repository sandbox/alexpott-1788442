<?php

/**
 * @file
 * Administration page callbacks and forms.
 */

/**
 * General configuration form for controlling the diff behaviour.
 */
function diff_admin_settings(&$form, &$form_state) {
  $config = config('diff.settings');
  $form['css_options'] = array(
    '#type' => 'select',
    '#title' => t('CSS Options'),
    '#default_value' => $config->get('css_options'),
    '#options' => array(
      'default' => t('Classic'),
      'boxes' => t('Boxes'),
    ),
    '#empty_option' => t('None'),
    '#description' => t('Alter the CSS used when displaying diff results.'),
  );
  $form['radio_behavior'] = array(
    '#type' => 'select',
    '#title' => t('Diff Radio Behavior'),
    '#default_value' => $config->get('radio_behavior'),
    '#options' => array(
      'simple' => t('Simple exclusion'),
      'linear' => t('Linear restrictions'),
    ),
    '#empty_option' => t('None'),
    '#description' => t('<em>Simple exclusion</em> means that users will not be able to select the same revision, <em>Linear restrictions</em> means that users can only select older or newer revisions of the current selections.'),
  );

  $options = drupal_map_assoc(array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
  $form['context.leading_lines'] = array(
    '#type' => 'select',
    '#title' => t('Leading context lines'),
    '#description' => t('This governs the number of unchanged leading context "lines" to preserve.'),
    '#default_value' => $config->get('context.leading_lines'),
    '#options' => $options,
  );
  $form['context.trailing_lines'] = array(
    '#type' => 'select',
    '#title' => t('Trailing context lines'),
    '#description' => t('This governs the number of unchanged trailing context "lines" to preserve.'),
    '#default_value' => $config->get('context.trailing_lines'),
    '#options' => $options,
  );
  $form['normalise_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Normalise text newlines before diff display'),
    '#description' => t('Convert all Windows and Mac newlines to a single newline, so diff only need to deal with one newline type. Unix users should have no need to enable this option.'),
    '#default_value' => $config->get('normalise_text', 0),
  );

  return system_config_form($form);
}

/**
 * Form submission handler for diff_admin_settings().
 *
 * @ingroup forms
 */
function diff_admin_settings_submit($form, &$form_state) {
  config('diff.settings')
    ->set('context.leading_lines', (int) $form_state['values']['context.leading_lines'])
    ->set('context.trailing_lines', (int) $form_state['values']['context.trailing_lines'])
    ->set('css_options', $form_state['values']['css_options'])
    ->set('normalise_text', (int) $form_state['values']['normalise_text'])
    ->set('radio_behaviour', $form_state['values']['radio_behaviour'])
    ->save();
}

/**
 * Global entity settings.
 */
function diff_admin_global_entity_settings($form, $form_state, $entity_type) {
  $config = config('diff.' . $entity_type);
  $form['entity_type'] = array(
    '#type' => 'hidden',
    '#value' => $entity_type,
  );
  $form['show_header'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show entity label header'),
		'#default_value' => $config->get('show_header'),
  );
  $form['admin_path'] = array(
    '#type' => 'checkbox',
    '#title' => t('Treat diff pages as administrative'),
    '#description' => t('Diff pages are treated as administrative pages by default, although it is up to each module to enforce this and to implement this optional setting.'),
	  '#default_value' => $config->get('admin_path'),
  );
  return system_config_form($form);
}

/**
 * Form submission handler for diff_admin_global_entity_settings().
 *
 * @ingroup forms
 */
function diff_admin_global_entity_settings_submit($form, &$form_state) {
  config('diff.' . $form_state['values']['entity_type'])
    ->set('admin_path', (int) $form_state['values']['admin_path'])
    ->set('show_header', (int) $form_state['values']['show_header'])
    ->save();
}
/**
 * Menu callback - provides an overview of Diff support and global settings.
 */
function diff_admin_field_overview() {
  $build['info'] = array(
    '#markup' => t('<p>This table provides a summary of the field type support found on the system. It is recommended that you use global settings whenever possible to configure field comparison settings.</p>'),
  );

  $header = array(t('Type'), t('Module'), t('Operations'));
  $rows = array();

  // Skip field types which have no widget types.
  $field_types = field_info_field_types();
  $widgets = array();
  foreach (field_info_widget_types() as $name => $widget_type) {
    foreach ($widget_type['field types'] as $widget_field_type) {
      if (isset($field_types[$widget_field_type])) {
        $widgets[$widget_field_type][$name] = $widget_type['label'];
      }
    }
  }

  foreach ($field_types as $field_name => $field_type) {
    if (!empty($widgets[$field_name])) {
      $row = array();
      $row[] = t('@field_label (%field_type)', array(
        '@field_label' => $field_type['label'],
        '%field_type' => $field_name,
      ));
      $row[] = $field_type['module'];
      $row[] = l(t('Global settings'), 'admin/config/content/diff/fields/' . $field_name);
      $rows[] = $row;
    }
  }

  $build['category_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('The system has no configurable fields.'),
  );
  return $build;
}

/**
 * Menu form callback for the field settings.
 */
function diff_admin_global_field_settings($form, $form_state, $type) {
  module_load_include('diff.inc', 'diff');

  $field_types = field_info_field_types();
  if (!isset($field_types[$type])) {
    drupal_set_message(t('Invalid field type.'), 'error');
    drupal_goto('admin/config/content/diff/fields');
  }
  $field_type = $field_types[$type];

  // Set the title to give more context to this page.
  drupal_set_title(t('Global settings for %label fields', array(
    '%label' => $field_type['label'],
  )), PASS_THROUGH);

  $variable_name = "diff_{$field_type['module']}_field_{$type}_default_options";
  $settings = variable_get($variable_name, array());
  $settings = _diff_field_default_settings($field_type['module'], $type, $settings);
  $func = $field_type['module'] . '_field_diff_options_form';
  if (function_exists($func) && $options_form = $func($type, $settings)) {
    $form[$variable_name] = $options_form;
  }
  $form[$variable_name]['#tree'] = TRUE;

  diff_global_settings_form($form[$variable_name], $form_state, $type, $settings);
  return system_settings_form($form);
}
