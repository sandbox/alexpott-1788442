<?php

namespace Drupal\diff\engine;

/**
 * @todo document
 * @private
 * @subpackage DifferenceEngine
 */
class DiffOpDelete extends DiffOp {
  var $type = 'delete';

  function __construct($lines) {
    $this->orig = $lines;
    $this->closing = FALSE;
  }

  function reverse() {
    return new DiffOpAdd($this->orig);
  }
}
