<?php

namespace Drupal\diff\engine;

/**
 * @todo document
 * @private
 * @subpackage DifferenceEngine
 */
class DiffOpAdd extends DiffOp {
  var $type = 'add';

  function __construct($lines) {
    $this->closing = $lines;
    $this->orig = FALSE;
  }

  function reverse() {
    return new DiffOpDelete($this->closing);
  }
}
