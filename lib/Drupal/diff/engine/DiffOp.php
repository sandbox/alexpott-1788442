<?php

namespace Drupal\diff\engine;

/**
 * @todo document
 * @private
 * @subpackage DifferenceEngine
 */
class DiffOp {
  var $type;
  var $orig;
  var $closing;

  function reverse() {
    trigger_error('pure virtual', E_USER_ERROR);
  }

  function norig() {
    return $this->orig ? sizeof($this->orig) : 0;
  }

  function nclosing() {
    return $this->closing ? sizeof($this->closing) : 0;
  }
}
