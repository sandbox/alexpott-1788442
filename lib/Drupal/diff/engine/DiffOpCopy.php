<?php

namespace Drupal\diff\engine;

/**
 * @todo document
 * @private
 * @subpackage DifferenceEngine
 */
class DiffOpCopy extends DiffOp {
  var $type = 'copy';

  function __construct($orig, $closing = FALSE) {
    if (!is_array($closing)) {
      $closing = $orig;
    }
    $this->orig = $orig;
    $this->closing = $closing;
  }

  function reverse() {
    return new DiffOpCopy($this->closing, $this->orig);
  }
}
