<?php

namespace Drupal\diff\engine;

/**
 * @todo document
 * @private
 * @subpackage DifferenceEngine
 */
class DiffOpChange extends DiffOp {
  var $type = 'change';

  function __construct($orig, $closing) {
    $this->orig = $orig;
    $this->closing = $closing;
  }

  function reverse() {
    return new DiffOpChange($this->closing, $this->orig);
  }
}
